import sys
import os
import numpy as np
from scipy import misc
from scipy import ndimage
# import pandas as pd
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from PIL import Image
from rotational_transform import Cartesianas_a_Cilindricas
from scipy import special
from scipy.interpolate import griddata


def Cargar_imagen(archivo):
    """
    Función que carga una imagen y la transforma en un np array
    """
    img = Image.open(archivo)   # Se abre la imagen
    image_nparray = np.array(img)       # Se crea un numpy array a partir de la imagen

    # Imagen "invertida"
    invimg = Image.fromarray(255 - image_nparray)
    # invimg.save('{}-inverted.png'.format(archivo))

    # Imagen borrosa
    blurred_array = ndimage.gaussian_filter(image_nparray, sigma=5)
    # blurred = Image.fromarray(blurred_array)
    # blurred.save('{}-blurred.png'.format(archivo))

    # Imagen rotada
    rotate_face = ndimage.rotate(image_nparray, 180)

    # Ìmagen invertida
    flip_face = np.flipud(image_nparray)

    fig, ax = plt.subplots(2, 2)
    ax[0, 0].imshow(invimg, cmap=plt.cm.gray)
    ax[0, 1].imshow(blurred_array, cmap=plt.cm.gray)
    ax[1, 0].imshow(rotate_face, cmap=plt.cm.gray)
    ax[1, 1].imshow(flip_face, cmap=plt.cm.gray)

    ax[0, 0].set(title='Colores invertidos')
    ax[0, 1].set(title='Borrosa')
    ax[1, 0].set(title='Rotada')
    ax[1, 1].set(title='Invertida')

    for ix in range(2):
        for iy in range(2):
            ax[ix, iy].xaxis.set_visible(False)
            ax[ix, iy].yaxis.set_visible(False)

    fig.suptitle('Imágenes manipuladas')

    # extraer la escala de la imagen y las dimensiones

    plt.show()

def Datos_Cil_a_Cart(ruta):
    """
    Función que toma puntos en coord cilíndricas y los transforma a cartesianas
    """
    archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    for iArchivo in archivos_trayectoria:
        # data = None
        ruta_archivo = '{}/{}'.format(ruta, iArchivo)
        # Cargar archivo de datos, es decir, puntos de trayectoria
        try:
            data = np.loadtxt(ruta_archivo)
            # print('Cargado archivo: {}'.format(iArchivo))
        except:
            print('\tExcepción: {}. Archivo: {}'.format(sys.exc_info()[0], iArchivo))
            # print('wtf!!')
            continue

        nPasos, nCol = data.shape

        # Array para guardar los puntos en las nuevas coordenadas
        data_cartesianas = np.zeros((nPasos, nCol))

        # print('tamaño de data_cilindricas: {}, {}'.format(nPasos, nCol))

        # Ciclo para transformar todos los puntos del arreglo
        for iPaso in range(nPasos):
            iPunto = np.array(data[iPaso, :])                   # Punto a considerar
            # print('punto en cartesianas: {}'.format(iPunto))
            coord_x, coord_y, coord_z = Cil_a_Cartesianas(iPunto)    # Punto inicial en coord cil
            # print('punto en cartesianas: {}'.format(iPunto_cart))
            data_cartesianas[iPaso, :] = np.array([coord_x, coord_y, coord_z, data[iPaso, 3], data[iPaso, 4], data[iPaso, 5], data[iPaso, 6]])

        # Guardar archivo con las nuevas coordenadas
        np.savetxt('{}_cartesianas.txt'.format(ruta_archivo.split('.')[0]), data_cartesianas, header='x, y, z, |B|, Bx, By, Bz')


def Cil_a_Cartesianas(punto):
    """
    Transformación de 'punto' de coordenadas cilíndricas
    a cartesianas: (r, phi ,z) -> (x, y, z)
    """

    coord_x = np.cos(punto[1])*punto[0]
    coord_y = np.sin(punto[1])*punto[0]

    # if punto[1] >= 0.0 and punto[1] < np.pi/2:
    #     coord_x = np.cos(punto[1])*punto[0]
    #     coord_y = np.sin(punto[1])*punto[0]
    #     # print('punto en el primer cuadrante {0:.2f}°'.format(phi*180/np.pi))
    #
    # elif punto[1] >= np.pi/2 and punto[1] < np.pi:
    #     coord_x = np.cos(punto[1])*punto[0]
    #     coord_y = np.sin(punto[1])*punto[0]
        # print('punto en el segundo cuadrante {0:.2f}°'.format(phi*180/np.pi))

    # elif vector[0] < 0 and vector[1] < 0:
    #     phi = 2*np.pi + np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el tercer cuadrante {0:.2f}°'.format(phi*180/np.pi))
    # else:
    #     phi = 2*np.pi + 2*np.pi + phi + 2*np.pi*(nRot - 1)
    #     # print('punto en el cuarto cuadrante {0:.2f}°'.format(phi*180/np.pi))

    # vector_cartesiano = np.array([coord_x, coord_y, punto[2]])

    # print('vector en cilíndricas: {}'.format(vector))

    # return vector_cartesiano
    return coord_x, coord_y, punto[2]


def Datos_a_cilindricas(ruta):
    """
    Función que toma las entradas en 'ruta' y transforma los puntos de
    coordenadas cartesianas a coordenadas cilíndricas
    """
    archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    for iArchivo in archivos_trayectoria:
        # data = None
        ruta_archivo = '{}/{}'.format(ruta, iArchivo)
        # Cargar archivo de datos, es decir, puntos de trayectoria
        try:
            data = np.loadtxt(ruta_archivo, delimiter=',', skiprows=1)
            print('Cargado archivo: {}'.format(iArchivo))
        except:
            print('\tExcepción: {}. Archivo: {}'.format(sys.exc_info()[0], iArchivo))
            # print('wtf!!')
            continue

        # if data is None:
        #     continue

        nPasos, nCol = data.shape

        nRot = 0        # Si nRot = 0 los puntos se mantienen en el rango [0, 2pi], i.e., una rotación.

        # Array para guardar los puntos en las nuevas coordenadas
        data_cilindricas = np.ndarray((nPasos, nCol), dtype=np.float)
        # print('tamaño de data_cilindricas: {}, {}'.format(nPasos, nCol))

        # Ciclo para transformar todos los puntos del arreglo
        for iPaso in range(nPasos):
            # Punto en coord cartesianas a transformar
            iPunto = np.array(data[iPaso, :])

            # Se obtienen las coordenadas cilíndricas del punto
            coord_r, coord_theta, coord_z = Cartesianas_a_Cilindricas(iPunto, nRot)

            # Para todos los pasos excepto el primero se ajusta el angulo
            # de acuerdo al número de rotaciones dadas
            if iPaso > 0:
                ang_punto_anterior = data_cilindricas[iPaso - 1, 1]
                # Se evalúa la componente toroidal para determinar el número de rotaciones dadas
                if abs(ang_punto_anterior - coord_theta) > np.pi:
                    # print('\n¡¡¡se ha completado una rotación!!! ang anterior: {:.5f}, actual: {:.5f}\n'.format(ang_punto_anterior*180/np.pi, coord_theta*180/np.pi))
                    nRot += 1
                    coord_r, coord_theta, coord_z = Cartesianas_a_Cilindricas(iPunto, nRot)
                # else:
                #     print('no se cumple la condición... :(')

            else:
                coord_r = iPunto[0]
                coord_theta = 0.0
                coord_z = iPunto[2]

            data_cilindricas[iPaso, :] = np.array([coord_r, coord_theta, coord_z, iPunto[3], iPunto[4], iPunto[5], iPunto[6]]) #iPunto_cil)

            # inicio = 1600
            # final = 1700
            #
            # if iPaso >= inicio and iPaso <= final:
            #     print('\t\tpunto en cartesianas: {:.5f}, {:.5f}, {:.5f}'.format(iPunto[0], iPunto[1], iPunto[2]))
            #     print('\t\tpunto en cilíndricas: {:.5f}, {:.5f}, {:.5f}'.format(coord_r, coord_theta*180/np.pi, coord_z))
            # if iPaso == final:
            #     break

        # Guardar archivo con las nuevas coordenadas
        # np.savetxt('{}_cilindricas.txt'.format(ruta_archivo.split('.')[-2]), data_cilindricas, header='r, theta [0, 2pi], z, |B|')
        np.savetxt('{}_cilindricas_inf.txt'.format(ruta_archivo.split('.')[-2]), data_cilindricas, header='r, theta [0, +inf[, z, |B|, Bx, By, Bz')


def Datos_a_toroidales(ruta):
    """
    Función que toma las entradas en 'ruta' y transforma los puntos de coordenadas
    cartesianas a coordenadas toroidales: (x, y, z) -> (r, phi, theta).
    phi es el ángulo toroidal y theta el ángulo poloidal
    """
    archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    for iArchivo in archivos_trayectoria:
        ruta_archivo = '{}/{}'.format(ruta, iArchivo)
        # Cargar archivo de datos, es decir, puntos de trayectoria
        try:
            data = np.loadtxt(ruta_archivo, skiprows=1)
            print('Cargado archivo: {}'.format(iArchivo))
        except:
            print('\tExcepción: {}'.format(sys.exc_info()[0]))
            # pass

        nPasos, nCol = data.shape

        nRot = 0        # Los puntos se mantienen en el rango [0, 2pi], i.e., una rotación.
        # Array para guardar los puntos en las nuevas coordenadas
        data_toroidales = np.ndarray((nPasos, nCol), dtype=np.float)
        # print('tamaño de data_cilindricas: {}, {}'.format(nPasos, nCol))

        # Ciclo para transformar todos los puntos del arreglo
        for iPaso in range(nPasos):

            # # Se obtienen las coordenadas cilíndricas del punto
            # coord_r, coord_theta, coord_z = Cartesianas_a_Cilindricas(iPunto, nRot)
            #
            # # Para
            #
            # else:
            #     coord_r = iPunto[0]
            #     coord_theta = 0.0
            #     coord_z = iPunto[2]
            #
            # data_cilindricas[iPaso, :] = np.array([coord_r, coord_theta, coord_z, iPunto[3], iPunto[4], iPunto[5], iPunto[6]]) #iPunto_cil)

            # Punto a considerar
            iPunto = np.array([data[iPaso, 0], data[iPaso, 1], data[iPaso, 2]])

            # Se obtienen las coordenadas cilíndricas del punto
            coord_r, coord_theta, coord_z = Cartesianas_a_Cilindricas(iPunto, nRot)

            # vector eje magnético para el punto a considerar
            iEje = np.array(Vector_eje_magnetico(iPunto))

            if iPaso > 0:
                ang_punto_anterior = data_toroidales[iPaso - 1, 1]

                # Se evalúa la componente toroidal para determinar el número de rotaciones dadas
                if abs(ang_punto_anterior - coord_theta) > np.pi:
                    # print('\n¡¡¡se ha completado una rotación!!! ang anterior: {:.5f}, actual: {:.5f}\n'.format(ang_punto_anterior*180/np.pi, coord_theta*180/np.pi))
                    nRot += 1
                    coord_r, coord_theta, coord_z = Cartesianas_a_Cilindricas(iPunto, nRot)
                # else:
                #     print('no se cumple la condición... :(')

            else:
                coord_r = iPunto[0]
                coord_theta = 0.0
                coord_z = iPunto[2]

            # iPunto_cil = np.array([coord_r, coord_theta, coord_z])    # Punto inicial en coord cil
            iAng_poloidal = Angulo_poloidal(iPunto, iEje)
            iPunto_tor = np.array([iPunto_cil[0], iPunto_cil[1], iAng_poloidal, data[iPaso, 3]])

            data_toroidales[iPaso, :] = np.array(iPunto_tor)

        # Guardar archivo con las nuevas coordenadas
        np.savetxt('{}_toroidales.txt'.format(ruta_archivo.split('.txt')[0]), data_toroidales, header='r, phi [0, 2pi], theta [-pi, pi], |B|')


def Hist_separacion_promedio(archivo):
    # Cargar archivo de datos, es decir, puntos de trayectoria
    data = np.loadtxt(archivo, skiprows=1)
    nPasos, _ = data.shape
    nRot = 0
    # Array para guardar las diferencia angular entre puntos consecutivos
    diferencias = np.zeros(nPasos, dtype = np.float64)

    # Ciclo para calcular las diferencias entre puntos consecutivos
    for iPaso in range(1, nPasos):
        iPunto = np.array(data[iPaso - 1, :])                   # Punto inicial
        fPunto = np.array(data[iPaso, :])                       # Punto final
        iPunto_cil = Cartesianas_a_Cilindricas(iPunto, nRot)    # Punto inicial en coord cil
        fPunto_cil = Cartesianas_a_Cilindricas(fPunto, nRot)    # Punto final en coord cil
        nDif = abs(iPunto_cil[1] - fPunto_cil[1])               # diferencia angular entre los puntos. En radianes

        # Condición para comprobar que los puntos cercanos a 2 pi son consecutivos
        if nDif > np.pi: # Si la diferencia angular es mayor que pi, se recalcula el ángulo
            nRot += 1                                               # Se adelanta un ciclo
            fPunto_cil = Cartesianas_a_Cilindricas(fPunto, nRot)    # se recalcula el ángulo
            nDif = abs(iPunto_cil[1] - fPunto_cil[1])               # S recalcula la diferencia angular
        #print('B_phi = A: {0:.3f}°, B: {1:.3f}°, Diff: {2:.5f}°, nRot = {3}'.format(iPunto_cil[1]*180/np.pi, fPunto_cil[1]*180/np.pi, nDif*180/np.pi, nRot))
        diferencias[iPaso - 1] = nDif

    hist = np.histogram(diferencias, bins = [0.000, 0.0030, 0.0032, 0.0034, 0.0036, 0.0038, 0.0040, 0.0042, 0.0044, 0.0046, 0.0048, 0.0050, 0.01])
    print('Histograma:\n\t{}'.format(hist))
    # fig, hist = plt.subplots(1, 1)
    # n, bins, patches = hist.hist(diferencias, bins=3, normed=1)
    # # hist.plot(bins, y, 'r--')
    # plt.show()


def Mapa_Campo(ruta_datos):
    """
    Función que toma los puntos para un corte toroidal seleccionados con la función diagrama Poincaré
    y grafica el campo magnético para ese corte.
    """

    # cambiar dimensiones basadas en la imagen de vmek
    # hacer las graficas de las particulas que divergen

    plt.close()
    fig, ax = plt.subplots(1, 1)
    theta_central = float(ruta_datos.split('_')[-1].split('.')[0])

    # assert theta_central is not string, 'what!'

    # print('{}'.format(theta_central))
    # archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    # Cargar archivo de datos, es decir, puntos de trayectoria
    try:
        data_cil = np.loadtxt(ruta_datos, skiprows=1) #, dtype={'names': ('r', 'theta', 'z'),'formats': ('f4', 'f4', 'f4')})
    except:
        # print('\tExcepción: {}. Archivo: {}'.format(sys.exc_info()[0], ruta_datos))
        return

    # Mapa del campo
    # Construcción de la grilla para el mapa
    # puntos_coord_r = np.linspace(0.14, 0.34, 1000)    # Puntos para la grilla en la coord r
    # puntos_coord_z = np.linspace(-0.1, 0.1, 1000)     # Puntos para la grilla en la coord z
    # grilla_r, grilla_z = np.meshgrid(puntos_coord_r, puntos_coord_z)

    # Interpolación de los datos
    # datos_interpolados = griddata((x,y),z,(xi,yi),method='linear')
    # datos_interpolados = griddata((data_cil[:, 0], data_cil[:, 2]), data_cil[:, 3], (grilla_r, grilla_z), method='linear')

    # # Gráfico de dispersión
    # ax.scatter(r_array[:, 1], r_array[:, 2], c=r_array[:, 3])
    # fig.colorbar(im, ax=ax)

    # Configuración del gráfico
    ax.set_xlabel('$r$ [m]')
    ax.set_ylabel('$z$ [m]')
    ax.set_title('Perfil de campo magnético\n$\\theta = {:.2f}$'.format(theta_central))
    ax.set_xlim([0.14, 0.34])
    ax.set_ylim([-0.1, 0.1])

    # Etiquetas
    xticks = ['{0:.02f}'.format(iy) for iy in np.linspace(0.14,0.34,11)]
    yticks = ['{0:.01f}'.format(iy) for iy in np.linspace(0,1,11)]
    ticks = np.linspace(0.14,0.34,11)

    ax.set_xticks(ticks)
    ax.set_xticklabels(xticks)
    # ax.set_yticks(ticks)
    # ax.set_yticklabels(yticks)

    # Graficación usando contourf
    # ax.contourf(grilla_r, grilla_z, datos_interpolados)

    # Graficación usando imshow
    # plt.imshow(datos_interpolados, vmin=data_cil[:, 3].min(), vmax=data_cil[:, 3].max(), aspect='equal', cmap='hot_r', origin='lower', extent=[data_cil[:, 0].min(), data_cil[:, 0].max(), data_cil[:, 2].min(), data_cil[:, 2].max()])

    # Graficación usando scatter
    plt.scatter(data_cil[:, 0], data_cil[:, 2], c=data_cil[:, 3]) #, s=8)
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('$|B|$', rotation = 90) #, fontsize=label_font_size)

    # Radio del stellarator
    circle = plt.Circle((0.24, 0), 0.095, color='k', fill=False)
    ax.add_artist(circle)

    plt.savefig('{}/{}/perfil_campo_theta_{:.2f}.png'.format(ruta_datos.split('/')[0], ruta_datos.split('/')[1], theta_central), format='png', bbox_inches='tight')
    # plt.show()

def Filtro_datos_Poincare(datos):
    """
    Se recibe un numpy array al que se le filtran algunos datos
    """
    # Se quitan los puntos con el campo mayor
    # datos[:, 3].min()


def Vector_eje_magnetico(vec_pos):
    """
    Determina el vector eje magnético respecto al punto dado. El vector se retorna en coordenadas cartesianas
    """
    r_eje_magnetico = 0.2477
    nRot = 0
    # print('vector pos antes: {}'.format(vec_pos))
    coord_r, coord_theta, coord_z = Cartesianas_a_Cilindricas(vec_pos, nRot)
    # print('vector pos despues: {}'.format(vec_pos))

    # Vector en coordenadas cartesianas
    vec_eje_magnetico = np.array([  r_eje_magnetico*np.cos(coord_theta),
                                    r_eje_magnetico*np.sin(coord_theta),
                                    0.0])

    # vec_eje_cil = np.array(Cartesianas_a_Cilindricas(vec_eje_magnetico, nRot))
    #
    # print('vector pos en cart: {}'.format(vec_pos))
    # print('vector pos en cil: {}'.format(vec_pos_cil))
    # print('vector eje magnético en cart: {}'.format(vec_eje_magnetico))
    # print('vector eje magnético en cil: {}'.format(vec_eje_cil))
    return vec_eje_magnetico


def Angulo_poloidal(vec_pos, vec_eje_magnetico):
    """
    Calcula el ángulo poloidal entre dos vectores cartesianos. Ángulo entre 'eje' y 'vec - eje'
    """
    vec_dif = np.array(vec_pos - vec_eje_magnetico)
    ang_poloidal = np.arccos(np.dot(vec_dif, vec_eje_magnetico)/(np.linalg.norm(vec_dif)*np.linalg.norm(vec_eje_magnetico)))

    if vec_pos[2] < 0:
        ang_poloidal = 2*np.pi - ang_poloidal
        # print('punto en el primer cuadrante {0:.2f}°'.format(ang_poloidal*180/np.pi))

    # elif vec_pos_cil[0] > r_eje_magnetico and vec_pos_cil[2] < 0:
    #     ang_poloidal = 2*np.pi - ang_poloidal
    #     print('punto en el segundo cuadrante {0:.2f}°'.format(ang_poloidal*180/np.pi))
    #
    # elif vec_pos_cil[0] < r_eje_magnetico and vec_pos_cil[2] > 0:
    #     phi = np.pi - ang_poloidal
    #     print('punto en el tercer cuadrante {0:.2f}°'.format(ang_poloidal*180/np.pi))
    # else:
    #     # vec_pos_cil[0] < r_eje_magnetico and vec_pos_cil[2] < 0:
    #     ang_poloidal = np.pi + ang_poloidal
    #     print('punto en el cuarto cuadrante {0:.2f}°'.format(ang_poloidal*180/np.pi))

    # print('angulo poloidal: {:.3f}'.format(ang_poloidal*180/np.pi))
    return ang_poloidal

if __name__ == '__main__':
    archivo = sys.argv[1]
    Hist_separacion_promedio(archivo)
