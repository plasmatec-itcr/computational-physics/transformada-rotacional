import sys
import os
import numpy as np
from scipy import special
# import pandas as pd
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata


def Grafico_iota_vs_r(archivo):
    data = np.loadtxt(archivo)
    figura, grafico = plt.subplots(1, 1)
    # gráfico de la trayectoria
    grafico.scatter(data[:, 0], data[:, 1], color='blue', s=1)
    grafico.set_title('Cálculo de iota vs radio para: \n$\phi$ = 90°, $\phi_{máx}$ = 90°')
    # grafico.set_xlim([0.25, 0.30])
    # grafico.set_ylim([0.25, 0.30])
    grafico.set_xlabel('radio [m]')
    grafico.set_ylabel('iota (%)')
    plt.show()


def Grafico_convergencia_iota(ruta_archivo1, ruta_archivo2):
    """
    Toma los datos de iota y los grafica respecto al ángulo phi_max
    """
    plt.close()
    data1 = np.loadtxt(ruta_archivo1)
    data2 = np.loadtxt(ruta_archivo2)

    figura, (graf1, graf2) = plt.subplots(1, 2, sharex = 'all', figsize = (10,5))
    # gráfico de la trayectoria
    figura.suptitle('Convergencia de iota vs $\phi_{máx}$')
    graf1.scatter(data1[:, 1], data1[:, 0], color='blue', s=0.001)
    graf1.set_title('Convergencia de iota entre $\phi_{max}$')
    graf1.set_xlabel('$\phi_{máx}$ [rad]')
    graf1.set_ylabel('iota [dimless]')

    graf2.scatter(data2[:, 1], data2[:, 0], color='green', s=0.001)
    graf2.set_title('Convergencia de iota sin división entre $\phi_{max}$')
    graf2.set_xlabel('$\phi_{máx}$ [rad]')
    graf2.set_ylabel('iota [dimless]')

    print('Gráfico para datos en: \n\t=> {}\n\t=> {}'.format(ruta_archivo1, ruta_archivo2))

    plt.savefig('Evolución iota-vs-phi_{}'.format(ruta_archivo1.split('_')[-1].split('.')[0])) #, format='eps', bbox_inches='tight')
    # plt.show()


def Grafico_B_vs_r(archivo):
    plt.close()
    data = np.loadtxt(archivo)
    figura, grafico = plt.subplots(1, 1)
    # gráfico de la trayectoria
    grafico.scatter(data[:, 0], data[:, 2], color='blue', s=1)
    grafico.set_title('Campo magnético vs radio para: \n$\phi$ = 0°')
    # grafico.set_xlim([0.25, 0.30])
    # grafico.set_ylim([0.25, 0.30])
    grafico.set_xlabel('radio [m]')
    grafico.set_ylabel('Campo magnético [T]')

    plt.show()


def Grafico_phi(datos):
    #plt.ion()
    figura, grafico = plt.subplots(1,1)
    nPasos, _ = datos.shape
    # nPasos = 2000
    # print('nPasos = {}'.format(nPasos))
    datos_np = np.zeros((nPasos, 3), dtype=np.float64)
    # print('afuera del ciclo')
    nRot = 0
    for ix in range(nPasos):
        # print('iteración {}'.format(ix))
        datos_np[ix][0] = datos['coord_x'][ix]
        datos_np[ix][1] = datos['coord_y'][ix]
        datos_np[ix][2] = datos['coord_z'][ix]

        datos_np[ix] = Cartesianas_a_Cilindricas(datos_np[ix], nRot)
        if abs(datos_np[ix - 1][1] - datos_np[ix][1]) > np.pi:
            nRot += 1
            datos_np[ix][0] = datos['coord_x'][ix]
            datos_np[ix][1] = datos['coord_y'][ix]
            datos_np[ix][2] = datos['coord_z'][ix]
            datos_np[ix] = Cartesianas_a_Cilindricas(datos_np[ix], nRot)

    #   print('\tphi = {}'.format(datos_np[ix][1]*180/np.pi))
    # print('número de rotaciones totales: {}'.format(nRot))

    # Gráfico estático
    # grafico.scatter(datos_np[:, 1], datos_np[:, 0]*np.cos(datos_np[:, 1]), s=0.01)
    # grafico.set_xlim((0, 100))
    # grafico.set_ylim((-1, 1))
    # plt.show()

    grafico.plot(datos_np[0, 1], datos_np[0, 0]*np.cos(datos_np[0, 1]), "x", color="blue")
    grafico.set_xlim((0, 2))
    grafico.set_ylim((-1, 1))

    # plt.ion()
    plt.show()
    # figura.canvas.draw()
    # plt.ioff()

    print('figura creada')

    # Gráfico animado
    for ix in range(500):
        # grafico.clear()
        grafico.plot(datos_np[ix, 1], datos_np[ix, 0]*np.cos(datos_np[ix, 1]), "x", color="blue")
        # grafico.set_xlim((0, 100))
        # grafico.set_ylim((-1, 1))

        # sleep(0.01)
        plt.savefig('fig-{}.png'.format(ix), format='png')
        # figura.canvas.draw()
    # plt.ioff()

        # print('iteración {}'.format(ix))

    # return datos_np
    # plt.ioff()


def Grafico3D(datos):
    """
    Gráfico en 3D de las trayectorias de las partículas en el plasma
    """

    plt.ion() # Buscar bien qué significa esto: interactive mode on

    # figura y subplot en una sola línea, ver si funciona para 3d
    # figura, grafico = plt.subplots(1,1, projection='3D')
    fig = plt.figure()

    ax = Axes3D(fig)
    # ax = fig.add_subplot(111, projection='3d')

    # Proyecciones sobre cada eje
    # ax.plot(datos['coord_x'], datos['coord_z'], 'r+', zdir='y', zs=1.5)
    # ax.plot(datos['coord_y'], datos['coord_z'], 'g+', zdir='x', zs=-0.5)
    # ax.plot(datos['coord_x'], datos['coord_y'], 'y+', zdir='z', zs=-0.3)

    # gráfico de la trayectoria
    ax.scatter(datos['coord_x'], datos['coord_y'], datos['coord_z'], s = 0.001)

    ax.set_xlim([-0.4, 0.4])
    ax.set_ylim([-0.4, 0.4])
    ax.set_zlim([-0.2, 0.2])

    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')

    plt.draw()
    plt.pause(1)
    #plt.show()


def Grafico_phi_vs_theta(ruta, r_central):
    """
    Script para hacer el grafico 'phi vs theta' usando todos los archivos de trayectorias dados por BS-Solctra
    usando un r constante (r_central)
    """
    archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    # nTrayectorias = len(archivos_trayectoria)   # se obtiene el número de archivos
    plt.close()
    r_eje_magnetico = 0.2477
    fig, ax = plt.subplots(1, 1)
    delta_r = 0.06                         # delta r en metros
    r_inicial = r_central - (delta_r/2.)      # r inicial en radianes
    r_final = r_central + (delta_r/2.)      # r_final en radianes

    # Lista para guardar los puntos que están en la región indicada
    puntos_r = []
    print('Iteración sobre los archivos en {}'.format(ruta))
    for iArchivo in archivos_trayectoria:
        ruta_archivo = '{}/{}'.format(ruta, iArchivo)
        # Cargar archivo de datos, es decir, puntos de trayectoria
        try:
            data_toroidales = np.loadtxt(ruta_archivo, skiprows=1)
        except:
            # print('\tExcepción: {}'.format(sys.exc_info()[0]))
            pass
        nPasos, _ = data_toroidales.shape
        # nRot = 0

        # Ciclo para determinar si el punto esta en el rango angular definido
        for iPaso in range(nPasos):
            # iPunto = np.array(data[iPaso - 1, :])                   # Punto inicial
            # # fPunto = np.array(data[iPaso, :])                       # Punto final
            # iPunto_cil = np.array(Cartesianas_a_Cilindricas(iPunto, nRot))    # Punto inicial en coord cil
            # fPunto_cil = Cartesianas_a_Cilindricas(fPunto, nRot)    # Punto final en coord cil
            # nDif = abs(iPunto_cil[1] - fPunto_cil[1])       # diferencia angular entre los puntos. En radianes

            iPunto_tor = np.array(data_toroidales[iPaso, :])

            # Si el punto tiene ángulo poloidal entre pi/2 y 3pi/2 se siguen estas condiciones:
            if iPunto_tor[2] < 3*np.pi/2. and iPunto_tor[2] > np.pi/2.:
                # Si el punto está en el rango radial dado, se guarda el punto
                if iPunto_tor[0] > r_eje_magnetico + r_final*np.cos(iPunto_tor[2]) and iPunto_tor[0] < r_eje_magnetico + r_inicial*np.cos(iPunto_tor[2]):
                    puntos_r.append(np.array(iPunto_tor))
            # Si el punto tiene ángulo poloidal entre -pi/2 y pi/2 se siguen estas condiciones
            else:
                # Si el punto está en el rango radial dado, se guarda el punto
                if iPunto_tor[0] < r_eje_magnetico + r_final*np.cos(iPunto_tor[2]) and iPunto_tor[0] > r_eje_magnetico + r_inicial*np.cos(iPunto_tor[2]):
                    puntos_r.append(np.array(iPunto_tor))

    # Fin del ciclo sobre archivos de trayectoria
    print('Fin de la iteración')
    # print('Lista de puntos en el intervalo [{0}, {1}]:\n{2}'.format(theta_inicial, theta_final, len(puntos_Poincare)))
    r_array = np.asarray(puntos_r)
    B_min = np.amin(r_array[:, 3])
    B_max = np.amax(r_array[:, 3])

    print('cantidad de puntos en el rango: {}'.format(len(r_array)))
    print('B mínimo: {}'.format(B_min))
    print('B máximo: {}'.format(B_max))

    # Construcción de la grilla para el gráfico
    puntos_coord_toroidal = np.linspace(0, 360, 721)*np.pi/180.    # Puntos para la grilla en la coord toroidal
    puntos_coord_poloidal = np.linspace(0, 180, 361)*np.pi/180.     # Puntos para la grilla en la coord poloidal
    grilla_toroidal, grilla_poloidal = np.meshgrid(puntos_coord_toroidal, puntos_coord_poloidal)

    # Interpolación de los datos
    # datos_interpolados = griddata((x,y),z,(xi,yi),method='linear')
    datos_interpolados = griddata((r_array[:, 1], r_array[:, 2]), r_array[:, 3], (grilla_toroidal, grilla_poloidal), method='linear')

    # Gráfico de dispersión
    # ax.scatter(r_array[:, 1], r_array[:, 2], c=r_array[:, 3])
    # fig.colorbar(im, ax=ax)

    # Configuración del gráfico
    ax.set_xlabel('$\\theta$ (poloidal) [°]')
    ax.set_ylabel('$\\phi$ (toroidal) [°]')
    ax.set_title('Grafico $\\phi$ vs $\\theta$\n$r = {:.4f}$'.format(r_central*180/np.pi))
    ax.set_ylim([-np.pi, np.pi])
    ax.set_xlim([0.0, 2*np.pi])
    ax.contourf(grilla_toroidal, grilla_poloidal, datos_interpolados)
    # ax.set_aspect('equal')
    # plt.savefig('diagrama_poincare_theta_{:.2f}.eps'.format(theta_central*180/np.pi), format='eps', bbox_inches='tight')
    plt.show()


def Grafico2_phi_vs_theta(ruta_archivo):
    """
    Script para hacer el grafico 'phi vs theta' usando un solo archivo de trayectoria dado por BS-Solctra. Se grafica su campo para todo phi y theta
    """
    # archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    # # nTrayectorias = len(archivos_trayectoria)   # se obtiene el número de archivos
    plt.close()
    # r_eje_magnetico = 0.2477
    fig, ax = plt.subplots(1, 1)
    # delta_r = 0.06                         # delta r en metros
    # r_inicial = r_central - (delta_r/2.)      # r inicial en radianes
    # r_final = r_central + (delta_r/2.)      # r_final en radianes

    # # Lista para guardar los puntos que están en la región indicada
    # puntos_r = []

    print('Iteración sobre el archivos {}'.format(ruta_archivo))
    # for iArchivo in archivos_trayectoria:
    # ruta_archivo = '{}/{}'.format(ruta, iArchivo)

    # Cargar archivo de datos, es decir, puntos de trayectoria
    # try:
    data_toroidales = np.loadtxt(ruta_archivo, skiprows=1)
    # except:
    #     print('\tExcepción: {}'.format(sys.exc_info()[0]))
    # # pass

    nPasos, _ = data_toroidales.shape
    # nRot = 0

    # # Ciclo para seleccionar los puntos en si el punto esta en el rango angular definido
    # for iPaso in range(nPasos):
    #     # iPunto = np.array(data[iPaso - 1, :])                   # Punto inicial
    #     # # fPunto = np.array(data[iPaso, :])                       # Punto final
    #     # iPunto_cil = np.array(Cartesianas_a_Cilindricas(iPunto, nRot))    # Punto inicial en coord cil
    #     # fPunto_cil = Cartesianas_a_Cilindricas(fPunto, nRot)    # Punto final en coord cil
    #     # nDif = abs(iPunto_cil[1] - fPunto_cil[1])       # diferencia angular entre los puntos. En radianes
    #
    #     iPunto_tor = np.array(data_toroidales[iPaso, :])
    #
    #     # Si el punto tiene ángulo poloidal entre pi/2 y 3pi/2 se siguen estas condiciones:
    #     if iPunto_tor[2] < 3*np.pi/2. and iPunto_tor[2] > np.pi/2.:
    #         # Si el punto está en el rango radial dado, se guarda el punto
    #         if iPunto_tor[0] > r_eje_magnetico + r_final*np.cos(iPunto_tor[2]) and iPunto_tor[0] < r_eje_magnetico + r_inicial*np.cos(iPunto_tor[2]):
    #             puntos_r.append(np.array(iPunto_tor))
    #     # Si el punto tiene ángulo poloidal entre -pi/2 y pi/2 se siguen estas condiciones
    #     else:
    #         # Si el punto está en el rango radial dado, se guarda el punto
    #         if iPunto_tor[0] < r_eje_magnetico + r_final*np.cos(iPunto_tor[2]) and iPunto_tor[0] > r_eje_magnetico + r_inicial*np.cos(iPunto_tor[2]):
    #             puntos_r.append(np.array(iPunto_tor))

    # Fin del ciclo sobre archivos de trayectoria
    # print('Fin de la iteración')
    # print('Lista de puntos en el intervalo [{0}, {1}]:\n{2}'.format(theta_inicial, theta_final, len(puntos_Poincare)))
    # r_array = np.asarray(puntos_r)

    B_min = np.amin(data_toroidales[:, 3])
    B_max = np.amax(data_toroidales[:, 3])

    # theta_min = np.amin(data_toroidales[:, 2])
    # theta_max = np.amax(data_toroidales[:, 2])

    # print('cantidad de puntos en el rango: {}'.format(nPasos))
    # print('B mínimo: {}'.format(B_min))
    # print('B máximo: {}'.format(B_max))
    # print('theta mínimo: {}'.format(theta_min*180/np.pi))
    # print('theta máximo: {}'.format(theta_max*180/np.pi))

    # Construcción de la grilla para el gráfico
    puntos_coord_toroidal = np.linspace(0, 360, 721)*np.pi/180.    # Puntos para la grilla en la coord toroidal
    puntos_coord_poloidal = np.linspace(0, 360, 361)*np.pi/180.     # Puntos para la grilla en la coord poloidal
    grilla_toroidal, grilla_poloidal = np.meshgrid(puntos_coord_toroidal, puntos_coord_poloidal)

    # Interpolación de los datos
    # datos_interpolados = griddata((x,y),z,(xi,yi),method='linear')
    datos_interpolados = griddata((data_toroidales[:, 1], data_toroidales[:, 2]), data_toroidales[:, 3], (grilla_toroidal, grilla_poloidal), method='linear')

    # Gráfico de dispersión
    # ax.scatter(r_array[:, 1], r_array[:, 2], c=r_array[:, 3])
    # fig.colorbar(im, ax=ax)

    # Configuración del gráfico
    ax.set_xlabel('$\\phi$ (poloidal) [°]')
    ax.set_ylabel('$\\theta$ (toroidal) [°]')
    ax.set_title('Superficie de B [T] $\\phi$ vs $\\theta$\nTrayectoria: {}'.format(ruta_archivo.split('/')[1]))
    ax.set_ylim([0.0, 2*np.pi])
    ax.set_xlim([0.0, 2*np.pi])
    plot = ax.contourf(grilla_toroidal, grilla_poloidal, datos_interpolados, vmin=B_min, vmax=B_max)
    fig.colorbar(plot, ax=ax) #, cmap=cmap, norm=norm, boundaries=bounds, ticks=[0.5,1.5,2.5,3.5],)
    # ax.set_aspect('equal')
    plt.savefig('phi-vs-theta_{}.eps'.format(ruta_archivo.split('/')[1].split('.')[0]), format='eps', bbox_inches='tight')
    # plt.show()


def Poincare_part_divergentes(ruta):
    """
    Función que grafica un poincaré para las partículas que divergen
    """
    plt.close()
    fig, ax = plt.subplots(1, 1)

    # Cargar archivo de datos, es decir, puntos de trayectoria
    # try:
    data_cil = np.loadtxt(ruta, skiprows=1) #, dtype={'names': ('r', 'theta', 'z'),'formats': ('f4', 'f4', 'f4')})
    # except:
        # print('\tExcepción: {}'.format(sys.exc_info()[0]))
        # return

    # Configuración del gráfico
    ax.set_xlabel('$r$ [m]')
    ax.set_ylabel('$z$ [m]')
    ax.set_title('Poincaré de una partícula divergente')
    ax.set_xlim([0.14, 0.34])
    ax.set_ylim([-0.1, 0.1])

    # Etiquetas
    xticks = ['{0:.02f}'.format(iy) for iy in np.linspace(0.14,0.34,11)]
    yticks = ['{0:.01f}'.format(iy) for iy in np.linspace(0,1,11)]
    ticks = np.linspace(0.14,0.34,11)

    ax.set_xticks(ticks)
    ax.set_xticklabels(xticks)
    # ax.set_yticks(ticks)
    # ax.set_yticklabels(yticks)

    plt.scatter(data_cil[:, 0], data_cil[:, 2], s=2)
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('$|B|$', rotation = 90) #, fontsize=label_font_size)

    # Radio del stellarator
    circle = plt.Circle((0.24, 0), 0.095, color='k', fill=False)
    ax.add_artist(circle)

    plt.show()
    print('Listo')


def Datos_region_Poincare(ruta, theta_central):
    """
    Esta función toma todos los archivos en 'ruta', es decir, datos en coordenadas
    cilíndricas y para un theta_central dado selecciona los puntos que están
    en un pequeño volumen
    """
    archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    theta_central = theta_central*np.pi/180.
    delta_theta = 0.005                             # delta theta en radianes
    theta_inicial = theta_central - (delta_theta/2.)    # ángulo inicial en radianes
    theta_final = theta_central + (delta_theta/2.)      # ángulo_final en radianes
    puntos_Poincare_totales = []                        # Lista que va a guardar todos los puntos seleccionados

    for iArchivo in archivos_trayectoria:
        ruta_archivo = '{}/{}'.format(ruta, iArchivo)
        # print('Procesando archivo: {}'.format(ruta_archivo))

        # Cargar archivo de datos, es decir, puntos de trayectoria
        # try:
        data_cil = np.loadtxt(ruta_archivo, skiprows=1)

        # except:
            # print('\tExcepción: {}'.format(sys.exc_info()[0]))
            # return
        nPasos, _ = data_cil.shape
        # nRot = 0
        # Lista para guardar los puntos que están en la región indicada
        # puntos_Poincare = []

        # Ciclo para determinar si el punto esta en el rango angular definido
        for iPaso in range(nPasos):
            iPunto_cil = np.array(data_cil[iPaso, :])
            # print(iPunto_cil)
            if iPunto_cil[1] > theta_inicial and iPunto_cil[1] < theta_final:
                # puntos_Poincare.append(np.array(iPunto_cil))
                puntos_Poincare_totales.append(iPunto_cil)
                # print(iPunto_cil)
            # else:
                # print('No hay puntos :(')
        # print('{}'.format(puntos_Poincare_totales))

    # print(poincare_array_total)
    poincare_array_total = np.asarray(puntos_Poincare_totales)
    np.savetxt('{}/puntos_poincare_theta_{:.1f}.txt'.format(ruta, theta_central*180/np.pi), poincare_array_total) #, header='puntos en el ángulo toroidal {},\ndatos tomados de: {}\nr, theta, z, B'.format(theta_central, archivos_trayectoria))     # finalmente se guardan los datos en un archivo de texto
    print('Listo! Proceso de archivos en {} terminado' .format(ruta))

def Diagrama_Poincare(ruta, theta_central):
    """
    Script para hacer diagrama de Poincaré tomando los datos de salida
    de la función datos región Poincaré usando todos los archivos de
    trayectorias dados por BS-Solctra ruta: ruta al archivos con los
    puntos en coord cilíndricas
    theta_central: ángulo para el corte toroidal en grados
    """
    archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    # print('{}'.format(archivos_trayectoria))
    # nTrayectorias = len(archivos_trayectoria)   # se obtiene el número de archivos
    archivos_trayectoria.sort()
    plt.close()
    fig, ax = plt.subplots(1, 1)
    # print('Poincaré para el ángulo {} en grados'.format(theta_central))
    theta_central = theta_central*np.pi/180.
    # print('Poincaré para el ángulo {} en radianes'.format(theta_central))
    delta_theta = 0.005                                 # delta theta en radianes
    theta_inicial = theta_central - (delta_theta/2.)    # ángulo inicial en radianes
    theta_final = theta_central + (delta_theta/2.)      # ángulo_final en radianes
    puntos_Poincare_totales = []                        # Lista que va a guardar todos los puntos seleccionados

    for iArchivo in archivos_trayectoria:
        ruta_archivo = '{}{}'.format(ruta, iArchivo)
        # print('{}'.format(ruta_archivo))
        # Cargar archivo de datos, es decir, puntos de trayectoria
        try:
            data_cil = np.loadtxt(ruta_archivo, skiprows=1)
            print('cargado archivo: {}'.format(ruta_archivo))
        except:
            print('\tExcepción: {}'.format(sys.exc_info()[0]))
            return
        nPasos, _ = data_cil.shape
        # nRot = 0
        # Lista para guardar los puntos que están en la región indicada
        puntos_Poincare = []

        # Ciclo para determinar si el punto esta en el rango angular definido
        for iPaso in range(nPasos):
            # iPunto = np.array(data[iPaso - 1, :])                   # Punto inicial
            # # fPunto = np.array(data[iPaso, :])                       # Punto final
            # iPunto_cil = np.array(Cartesianas_a_Cilindricas(iPunto, nRot))    # Punto inicial en coord cil
            # fPunto_cil = Cartesianas_a_Cilindricas(fPunto, nRot)    # Punto final en coord cil
            # nDif = abs(iPunto_cil[1] - fPunto_cil[1])       # diferencia angular entre los puntos. En radianes
            iPunto_cil = np.array(data_cil[iPaso, :])
            # if iPunto_cil[1] > theta_inicial and iPunto_cil[1] < theta_final:
            if iPunto_cil[1] < theta_inicial and iPunto_cil[1] > theta_final:
                puntos_Poincare.append(np.array(iPunto_cil))
                # puntos_Poincare_totales.append(np.array(iPunto_cil))

        # print('Lista de puntos en el intervalo [{0}, {1}]:\n{2}'.format(theta_inicial, theta_final, len(puntos_Poincare)))
        poincare_array = np.asarray(puntos_Poincare)
        print('{} arreglo\n\n'.format(poincare_array))
        # Grafica del diagrama de Poincare
        ax.scatter(poincare_array[:, 0], poincare_array[:, 2], s=0.02)#, c='r') # s = 0.002
        # print('Procesado archivo: {}'.format(ruta_archivo))

    # poincare_array_total = np.asarray(puntos_Poincare_totales)
    # np.savetxt('puntos_poincare_ang_{:.2f}.txt'.format(theta_central*180/np.pi), poincare_array_total) #, header='puntos en el ángulo toroidal {},\ndatos tomados de: {}\nr, theta, z, B'.format(theta_central, archivos_trayectoria))     # finalmente se guardan los datos en un archivo de texto

    # Fin del ciclo sobre archivos de trayectoria
    ax.scatter(0.2477, 0, color='black', s=0.002)
    ax.set_xlabel('R [m]')
    ax.set_ylabel('z [m]')
    ax.set_title('Diagrama de Poincaré\n$\\theta = {:.2f}$'.format(theta_central*180/np.pi))
    ax.set_ylim([-0.1, 0.1])
    ax.set_xlim([0.14, 0.34])
    ax.set_aspect('equal')
    # plt.savefig('diagrama_poincare_theta_{:.2f}.png'.format(theta_central*180/np.pi), format='png', bbox_inches='tight')
    plt.show()


def indv_plot_polvstor(filename,main_dir,save_dir):
    """
    Entradas: filename -- str; Nombre del archivo a analizar.
            , main_dir -- str; Path del directorio donde se está ejecutando el código.
            , save_dir -- str; Path del directorio donde se guardan los graficos.
    Salidas:  single_______.png imagen del gráfico hecho a partir de los datos
                encontrados en filename
    Descripción: Toma los archivos con datos del directorio "work_dir" y los analiza para
                 generar graficas de B vs theta vs phi, y a su vez guardar estos archivos en "save_dir".

    Ej: indv_plot_polvstor(filename,"./results_6945","./graficos")
    """
    lineas = int(subprocess.check_output(['wc', '-l', filename]).split()[0])-2
    print('\t\tNúmero de lineas de archivo '+filename+': {}'.format(lineas))
    data = np.loadtxt(filename, dtype=float, skiprows=1, delimiter='\t', max_rows=lineas)
    shp = data.shape
    _, phi, theta, B, _, _, _ = data.T

    for i in range(shp[0]):
        if theta[i] > np.pi :
            theta[i] = theta[i]-2*np.pi

    theta = theta*180/np.pi
    phi = phi*180/np.pi
    os.chdir(save_dir)
    plt.scatter(phi, theta, marker=".", c=B, cmap=cmx.jet)
    plt.title("Magnitud de B vs posicion angular "+filename)
    plt.xlabel(r"Ángulo toroidal $\phi$")
    plt.ylabel(r"Ángulo poloidal $\theta$")
    plt.xticks(np.linspace(0,360,7))
    plt.yticks(np.linspace(-180,180,7))
    cbar = plt.colorbar()
    cbar.set_label("Magnitud de campo magnético B")
    plt.savefig("single"+str(filename)+".png")
    plt.close()
    os.chdir(main_dir)


def plot_polvstor(work_dir,save_dir):
    """
    Entradas: work_dir -- str; Path del directorio donde se encuentran los archivos de
              trayectorias
            , save_dir -- str; Path del directorio donde se guardan los graficos.
    Salidas:  single_______.png imagen del gráfico hecho a partir de los datos
              encontrados en filename
    Descripción: Toma los archivos con datos del directorio "work_dir" y los analiza por
                 por medio de la función indv_plot_polvstor para generar graficas de B vs theta vs phi, y a su vez guardar estos archivos en "save_dir".
    """
    print('Graficando archivos en {}'.format(work_dir))
    lista_archivos = os.listdir(work_dir)	#Lista de archivos
    lista_archivos.sort()
    main_dir = os.getcwd()	#Directorio principal
    print("\tAccesando al directorio: "+work_dir+"\n")

    for iArch in lista_archivos:
        os.chdir(work_dir)
        print("\tAnalizando archivo "+iArch+"...")
        indv_plot_polvstor(iArch, main_dir, save_dir)

    os.chdir(main_dir)
    print("\tGráficos en"+work_dir+"listos.")


def histograma_iota(archivo):
    '''
    Entradas:
    Salidas:
    Descripción: toma el archivo de datos de iota y genera un histograma_iota
    '''
    plt.close()
    data = np.loadtxt(archivo, usecols=(1,2))
    print('data:\n{}'.format(data))
    data_filtrado = [data[i, 0] for i in range(len(data)) if data[i, 1]==499999]
    print('data filtrado:\n{}'.format(data_filtrado))
    cuentas, cajas = np.histogram(data_filtrado)
    print('cuentas:\n{}'.format(cuentas))
    print('cajas:\n{}'.format(cajas))
    plt.hist(data_filtrado, bins=cajas, density=True)  # arguments are passed to np.histogram
    plt.title("Histogram with 'auto' bins")
    plt.xlabel('iota')
    plt.ylabel('frecuencia')
    plt.show()



#------------------------------ECRH surface mapping ------------------------------#

def Plasma_Poincare_map(file_dir, angle_cut):
	"""
	Entradas:file_dir (string) directorio del archivo con los datos de las superficies 				magneticas del plasma.

			 angle_cut (float) ángulo en grados para el corte de la sección de poincaré. 

	Salidas: fig (matplotlib.pyplot figure) figura de mapa de poincare generado
			 ax (matplotlib.pyplot axis) axis de la figura de mapa de poincare

			 phi_up (float) limite superior de la pequeña caja que contiene las intersecciones de la trayectoria con el plano dado por phi = angle_cut

			 phi_down (float) limite inferior de la pequeña caja que contiene las intersecciones de la trayectoria con el plano dado por phi = angle_cut

	Descrip: Función que toma las trayetorias generadas por el módulo BSOLCTRA y grafica un mapa de poincaré asociado al ángulo deseado. 

	NOTA: Si se utiliza en Linux no hay problema, con Anaconda presenta un WinError2, filenotfound en la linea []
	"""
	print('Graficando archivos en {}'.format(file_dir))

	main_dir = os.getcwd() 					
	#Se guarda el path del directorio inicial puesto que se necesitan accesar a los directorios deseados. 
	
	list_archiv = os.listdir(file_dir)
	list_archiv.sort()

	angle_cut = angle_cut*np.pi/180
	dphi = 0.01
	phi_up = angle_cut + dphi/2
	phi_down = angle_cut - dphi/2
	R0 = 0.2477 #Radio característico del SCR-1

	plotcoords = list()
	fig, ax = plt.subplots(1,1)
	os.chdir(file_dir)
	
	for iArch in list_archiv: #Se itera sobre los archivos en el directorio

		lines = int(subprocess.check_output(['wc', '-l', iArch]).split()[0])-2

		data = np.loadtxt(iArch, dtype=float, skiprows=1, delimiter='\t', max_rows=lines)

		#Cilindrical Toroidal Transform
		iRange, _ = data.shape
	
		for i in range(iRange):

			if data[i,1] > phi_down and data[i,1] < phi_up:
				rtor = data[i,0]
				thetator = data[i,2]
				R = (R0+rtor*np.cos(thetator))
				z = rtor*np.sin(thetator)
				plotcoords.append([R,z])
		
		plotcoord = np.array(plotcoords)
		if plotcoord.shape[0]==0:
			continue
		else:
			radius, z = plotcoord.T
			ax.scatter(radius, z, marker='.', s=1.3,)

	os.chdir(main_dir)
	ax.scatter(0.2477, 0, color='black', s=0.002)
	ax.set_xlabel('R [m]')
	ax.set_ylabel('z [m]')
	ax.set_title('Diagrama de Poincaré\n$\\theta = {:.2f}$'.format(angle_cut*180/np.pi))
	ax.set_xlim([0.18, 0.31])
	#plt.savefig("poincare"+str(angle_cut*180/np.pi)+".png")

	return fig, ax, phi_up, phi_down


def ECRHsurfmap(work_dir):
	"""
	Entradas: work_dir (string) directorio en el cual se encuentran los datos de las 			  lineas de campo magnetico en coordenadas toroidales.
	Salidas: grafico de la linea de ajuste para los puntos donde se es óptimo "calentar" 		  el plasma por medio del método de ECRH. 
	Descrp: A partir de los datos de las lineas de campo magnetico dadas por el BSOLCTRA 
			para el SCR-1, encontrar los datos que representan una superficie que denota el mejor espacio para utilizar el método de ECRH para el calentamiento por RF. 

	
	"""
	R0 = 0.2477					#radio característico SCR-1(magnético)
	heatfreq = 2.45*10**9 		#frecuencia de calentamiento
	B2 = (2*np.pi*heatfreq*9.10938291*10**-31)/((1.60217*10**-19)*2)

	surfdata2 = list() 

	print('Analizando archivos en {}'.format(work_dir))
	lista_archivos = os.listdir(work_dir)
	lista_archivos.sort()
	main_dir = os.getcwd()
	print("\tAccesando al directorio: "+work_dir+"\n")

	os.chdir(work_dir)

	#Se compara para cada archivo si su valor de magnitud es igual a B2
	for iArch in lista_archivos:
		
		print("\tAnalizando archivo "+iArch+"...")
		
		#Analisis de los datos

		lineas = int(subprocess.check_output(['wc', '-l', iArch]).split()[0])-2
		print('\t Cantidad de lineas: {}'.format(lineas))
		data = np.loadtxt(iArch, dtype=float, skiprows=1, delimiter='\t', max_rows=lineas)
		shp = data.shape
		Bintwidth = 0.0002
		for i in range(shp[0]):
			if B2-Bintwidth/2 < data[i,3] and B2+Bintwidth/2 > data[i,3]:
				surfdata2.append(data[i])
		
	
	surf2 = np.array(surfdata2)
	os.chdir(main_dir)	 

	#Generar gráfico poincaré
	angle = 0#float(input("Ingresar ángulo para la sección: "))

	#Se grafica primero un mapa de poincare en el angulo dado para mostrar la superficie en el angulo dado
	fig, ax, phi_up, phi_down = Plasma_Poincare_map(work_dir, angle)

	iR2, _ = surf2.shape

	coords2 = list()

	for i in range(iR2):
		if phi_down < surf2[i,1] and surf2[i,1] < phi_up:
			R = R0+surf2[i,0]*np.cos(surf2[i,2])
			z = surf2[i,0]*np.sin(surf2[i,2])
			coords2.append([R,z])

	coords2 = np.array(coords2)
	
	R2, Z2 = coords2.T

	fit = np.polyfit(Z2, R2, 3)		

	#dependiendo de la orientación es mejor intercambiar Z por R en np.polyfit, de esta manera np.polyfit(R2,Z2,3)

	points = np.linspace(min(Z2),max(Z2))
	fitpoly = np.polyval(fit ,points)

	#ax.scatter(R2, Z2, marker=".", color="blue", label="2do harmónico"), graficación de puntos de trayectoria que cumplen con la condición. 

	ax.plot(fitpoly, points, color="red")
	plt.savefig("poincareEH"+str(angle)+".png", dpi=300)
	plt.close()

#----------------------------------------------------------------------------------------