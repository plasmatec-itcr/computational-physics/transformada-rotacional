# Archivos transformada rotacional

## rotational transform.py:
Script con la implementación del método para el cálculo de la transformada rotacional tomado del artículo [Calculating Rotational Transform Following Field Lines](https://www.jstage.jst.go.jp/article/jspf/79/4/79_4_321/_pdf)

# Proceso

* Descargar los datos del cluster kabré\n",
* Transformar los coordenadas cartesianas de las líneas de flujo magnético a coordenadas cilíndricas
* Seleccionar los puntos que corresponden al ángulo a estudiar
* Correr transformada rot.py